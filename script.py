# Abrir archivo

exit = 0

while (exit == 0):
        print()
        print("1) Buscar por el archivo palabras.txt")
        print("2) Buscar una palabra")
        print("3) Salir")
        try:
                option = int(input("Seleccion: "))
        except:
                option = 0

        print()
        # ejecuta la opcion escogida
        if option == 1:
                palabras = open("palabras.txt") # abrir archivo de palabras a buscar
                ocurrencias = {} # inicializa el diccionario
                for linea in palabras:
                        palabra_a_buscar = linea.rstrip() # elimina espacios en blanco y obtiene la palabra
                        ocurrencias[palabra_a_buscar] = 0 # inicializa la palabra a buscar en el diccionario
                        archivo = open("encargos.txt") # abre el archivo donde vamos a buscar
                        for line in archivo:
                                ocurrencias[palabra_a_buscar] += line.count(palabra_a_buscar) #actualiza el contador de veces encontradas
                        archivo.close() # cierra el archivo donde buscamos

                palabras.close() # cierra el archivo de palabras a buscar
                for clave in ocurrencias:
                        print("Ocurrencias de", clave + ":", ocurrencias[clave]) # imprime las palabras encontradas con ocurrencias
        elif option == 2:
                palabra = input("Ingrese la palabra que desea buscar: ") # solicita la palabra a buscar
                archivo = open("encargos.txt") # ver comentarios del segundo FOR de arriba
                count = 0
                for line in archivo:
                        count += line.count(palabra)
                archivo.close()
                print("Ocurrencias de", palabra + ":", count)
        elif option == 3:
                exit = 1
        else:
                print("Escoja una opcion del menu")
print("Gracias por usar el sistema de los papis")
